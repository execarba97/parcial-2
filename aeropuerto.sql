USE [master]
GO
/****** Object:  Database [AEROPUERTO]    Script Date: 11/17/2020 11:03:09 PM ******/
CREATE DATABASE [AEROPUERTO]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'AEROPUERTO', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\AEROPUERTO.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'AEROPUERTO_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\AEROPUERTO_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [AEROPUERTO] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [AEROPUERTO].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [AEROPUERTO] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [AEROPUERTO] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [AEROPUERTO] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [AEROPUERTO] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [AEROPUERTO] SET ARITHABORT OFF 
GO
ALTER DATABASE [AEROPUERTO] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [AEROPUERTO] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [AEROPUERTO] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [AEROPUERTO] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [AEROPUERTO] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [AEROPUERTO] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [AEROPUERTO] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [AEROPUERTO] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [AEROPUERTO] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [AEROPUERTO] SET  DISABLE_BROKER 
GO
ALTER DATABASE [AEROPUERTO] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [AEROPUERTO] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [AEROPUERTO] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [AEROPUERTO] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [AEROPUERTO] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [AEROPUERTO] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [AEROPUERTO] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [AEROPUERTO] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [AEROPUERTO] SET  MULTI_USER 
GO
ALTER DATABASE [AEROPUERTO] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [AEROPUERTO] SET DB_CHAINING OFF 
GO
ALTER DATABASE [AEROPUERTO] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [AEROPUERTO] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [AEROPUERTO] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [AEROPUERTO] SET QUERY_STORE = OFF
GO
USE [AEROPUERTO]
GO
/****** Object:  Table [dbo].[AVIONES]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AVIONES](
	[id] [int] NOT NULL,
	[marca] [varchar](50) NULL,
	[patente] [varchar](50) NULL,
	[capacidad] [varchar](50) NULL,
	[aerolinea] [varchar](50) NULL,
 CONSTRAINT [PK_Aviones] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BOLETOS]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BOLETOS](
	[id] [int] NOT NULL,
	[idPasajero] [int] NULL,
	[idDestino] [int] NULL,
	[idOrigen] [int] NULL,
	[precio] [float] NULL,
	[salida] [date] NULL,
	[llegada] [date] NULL,
	[idVuelo] [int] NULL,
	[estado] [int] NULL,
 CONSTRAINT [PK_Boletos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DESTINOS]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DESTINOS](
	[id] [int] NOT NULL,
	[pais] [varchar](50) NULL,
	[ciudad] [varchar](50) NULL,
	[coordSur] [varchar](50) NULL,
	[coordNorte] [varchar](50) NULL,
	[estado] [int] NULL,
 CONSTRAINT [PK_DESTINOS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ESCALAS]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ESCALAS](
	[id] [int] NOT NULL,
	[idDestino] [int] NULL,
	[tLlegada] [datetime] NULL,
	[tPartida] [datetime] NULL,
	[idVuelo] [int] NULL,
 CONSTRAINT [PK_ESCALAS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PASAJEROS]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PASAJEROS](
	[id] [int] NOT NULL,
	[nombre] [varchar](50) NULL,
	[apellido] [varchar](50) NULL,
	[fechaNacimiento] [date] NULL,
	[numeroPasaporte] [numeric](18, 0) NULL,
	[estado] [int] NULL,
 CONSTRAINT [PK_PASAJERO] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VUELOS]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VUELOS](
	[id] [int] NOT NULL,
	[estado] [int] NULL,
	[tEmbarco] [datetime] NULL,
	[tLlegada] [datetime] NULL,
	[idAvion] [int] NULL,
	[tipo] [varchar](50) NULL,
	[idCiudadOrigen] [int] NOT NULL,
	[idCiudadDestino] [int] NOT NULL,
 CONSTRAINT [PK_VUELOS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BOLETOS]  WITH CHECK ADD  CONSTRAINT [FK_BOLETOS_DESTINOS] FOREIGN KEY([idDestino])
REFERENCES [dbo].[DESTINOS] ([id])
GO
ALTER TABLE [dbo].[BOLETOS] CHECK CONSTRAINT [FK_BOLETOS_DESTINOS]
GO
ALTER TABLE [dbo].[BOLETOS]  WITH CHECK ADD  CONSTRAINT [FK_BOLETOS_DESTINOS1] FOREIGN KEY([idOrigen])
REFERENCES [dbo].[DESTINOS] ([id])
GO
ALTER TABLE [dbo].[BOLETOS] CHECK CONSTRAINT [FK_BOLETOS_DESTINOS1]
GO
ALTER TABLE [dbo].[BOLETOS]  WITH CHECK ADD  CONSTRAINT [FK_BOLETOS_PASAJEROS] FOREIGN KEY([idPasajero])
REFERENCES [dbo].[PASAJEROS] ([id])
GO
ALTER TABLE [dbo].[BOLETOS] CHECK CONSTRAINT [FK_BOLETOS_PASAJEROS]
GO
ALTER TABLE [dbo].[BOLETOS]  WITH CHECK ADD  CONSTRAINT [FK_BOLETOS_VUELOS] FOREIGN KEY([idVuelo])
REFERENCES [dbo].[VUELOS] ([id])
GO
ALTER TABLE [dbo].[BOLETOS] CHECK CONSTRAINT [FK_BOLETOS_VUELOS]
GO
ALTER TABLE [dbo].[ESCALAS]  WITH CHECK ADD  CONSTRAINT [FK_ESCALAS_DESTINOS] FOREIGN KEY([idDestino])
REFERENCES [dbo].[DESTINOS] ([id])
GO
ALTER TABLE [dbo].[ESCALAS] CHECK CONSTRAINT [FK_ESCALAS_DESTINOS]
GO
ALTER TABLE [dbo].[ESCALAS]  WITH CHECK ADD  CONSTRAINT [FK_ESCALAS_VUELOS] FOREIGN KEY([idVuelo])
REFERENCES [dbo].[VUELOS] ([id])
GO
ALTER TABLE [dbo].[ESCALAS] CHECK CONSTRAINT [FK_ESCALAS_VUELOS]
GO
ALTER TABLE [dbo].[VUELOS]  WITH CHECK ADD  CONSTRAINT [FK_VUELOS_AVIONES] FOREIGN KEY([idAvion])
REFERENCES [dbo].[AVIONES] ([id])
GO
ALTER TABLE [dbo].[VUELOS] CHECK CONSTRAINT [FK_VUELOS_AVIONES]
GO
ALTER TABLE [dbo].[VUELOS]  WITH CHECK ADD  CONSTRAINT [FK_VUELOS_DESTINOS] FOREIGN KEY([idCiudadOrigen])
REFERENCES [dbo].[DESTINOS] ([id])
GO
ALTER TABLE [dbo].[VUELOS] CHECK CONSTRAINT [FK_VUELOS_DESTINOS]
GO
ALTER TABLE [dbo].[VUELOS]  WITH CHECK ADD  CONSTRAINT [FK_VUELOS_DESTINOS1] FOREIGN KEY([idCiudadDestino])
REFERENCES [dbo].[DESTINOS] ([id])
GO
ALTER TABLE [dbo].[VUELOS] CHECK CONSTRAINT [FK_VUELOS_DESTINOS1]
GO
/****** Object:  StoredProcedure [dbo].[AltaAvion]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[AltaAvion]
@marca varchar(50),
@patente varchar(50),
@capacidad varchar(50),
@aerolinea varchar(50)
as
begin
	declare @id int
	set @id = (SELECT ISNULL(MAX(id), 0)+1 FROM dbo.[AVIONES])
	INSERT INTO [dbo].[AVIONES] ([id],[marca],[patente],[capacidad],[aerolinea])
    VALUES(@id,@marca,@patente,@capacidad,@aerolinea)
end
GO
/****** Object:  StoredProcedure [dbo].[AltaBoleto]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[AltaBoleto]
@idPasajero int,
@idDestino int,
@idOrigen int,
@precio float,
@salida date,
@llegada date,
@idvuelo int
as
begin
	declare @id int
	set @id = (SELECT ISNULL(MAX(id), 0)+1 FROM dbo.[BOLETOS])
	INSERT INTO [dbo].[BOLETOS] ([id],[idPasajero],[idDestino],[idOrigen],[precio],[salida]
           ,[llegada],[idVuelo],[estado])
    VALUES
           (@id,
            @idPasajero,
            @idDestino,
            @idOrigen,
            @precio,
			@salida,
			@llegada,
			@idvuelo,
			1)

end
GO
/****** Object:  StoredProcedure [dbo].[AltaDestino]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[AltaDestino]
@pais varchar(50),
@ciudad varchar(50),
@coordSur varchar(50),
@coordNorte varchar(50)
as
begin
	declare @id int, @estado int
	set @estado = 1
	set @id = (SELECT ISNULL(MAX(id), 0)+1 FROM dbo.[DESTINOS])
	INSERT INTO [dbo].[DESTINOS] ([id],[pais],[ciudad],[coordSur],[coordNorte],[estado])
    VALUES(@id,@pais,@ciudad,@coordSur,@coordNorte,@estado)
end
GO
/****** Object:  StoredProcedure [dbo].[AltaEscala]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[AltaEscala]
@idDestino int,
@tLlegada datetime,
@tPartida datetime,
@idVuelo int
as
begin
	declare @id int, @fecha1 datetime, @fecha2 datetime

	set @id = (SELECT ISNULL(MAX(id), 0)+1 FROM dbo.[ESCALAS])
	set @fecha1 = (select convert(varchar, @tPartida, 113))
	set @fecha2 = (select convert(varchar, @tLlegada, 113))
	
	INSERT INTO [dbo].[ESCALAS]
           ([id]
           ,[idDestino]
           ,[tLlegada]
           ,[tPartida]
           ,[idVuelo])
     VALUES
           (@id,
			@idDestino,
			@fecha2,
			@fecha1,
			@idVuelo)

end


GO
/****** Object:  StoredProcedure [dbo].[AltaPasajero]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[AltaPasajero]
@nombre varchar(50),
@apellido varchar(50),
@fechaNacimiento date,
@numeroPasaporte numeric(18,0)
as
begin
	declare @id int, @estado int
	set @estado = 1
	set @id = (SELECT ISNULL(MAX(id), 0)+1 FROM dbo.[PASAJEROS])
	INSERT INTO [dbo].[PASAJEROS]
           ([id]
           ,[nombre]
           ,[apellido]
           ,[fechaNacimiento]
           ,[numeroPasaporte]
           ,[estado])
     VALUES
           (@id,
			@nombre,
			@apellido,
			@fechaNacimiento,
			@numeroPasaporte,
			@estado)
	
end
GO
/****** Object:  StoredProcedure [dbo].[AltaVuelo]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[AltaVuelo]
@tEmbarco varchar(50),
@tLlegada varchar(50),
@idAvion int,
@tipo varchar(50),
@idCiudadOrigen int,
@idCiudadDestino int
as
begin
	declare @id int, @estado int, @fecha1 datetime, @fecha2 datetime
	set @estado = 1
	set @id = (SELECT ISNULL(MAX(id), 0)+1 FROM dbo.[VUELOS])
	set @fecha1 = (select convert(varchar, @tEmbarco, 113))
	set @fecha2 = (select convert(varchar, @tLlegada, 113))

	INSERT INTO [dbo].[VUELOS]
           ([id],[estado],[tEmbarco],[tLlegada],[idAvion],[tipo],[idCiudadOrigen],[idCiudadDestino])
    VALUES
           (@id,
            @estado,
		    @fecha1,
		    @fecha2,
            @idAvion,
            @tipo,
            @idCiudadOrigen,
		    @idCiudadDestino)
end
GO
/****** Object:  StoredProcedure [dbo].[BajaDestino]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[BajaDestino]
@id int
as
begin
	declare @estado int
	set @estado = 0
	UPDATE [dbo].[DESTINOS]
	SET [estado] = @estado
	WHERE id = @id

end
GO
/****** Object:  StoredProcedure [dbo].[BajaPasajero]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[BajaPasajero]
@id int
as
begin
	declare @estado int
	set @estado = 0
	UPDATE [dbo].[PASAJEROS]
	SET estado = @estado
	WHERE id = @id
	
	UPDATE dbo.BOLETOS
	set estado = 0
	where idPasajero = @id
end
GO
/****** Object:  StoredProcedure [dbo].[CancelarVuelo]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[CancelarVuelo]
@id int
as
begin
	UPDATE [dbo].[VUELOS]
    SET [estado] = 0
	WHERE id = @id

	UPDATE [dbo].BOLETOS
    SET [estado] = 0
	WHERE idVuelo = @id
end
GO
/****** Object:  StoredProcedure [dbo].[ListarAviones]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ListarAviones]
as
begin
	SELECT [id]
      ,[marca]
      ,[patente]
      ,[capacidad]
      ,[aerolinea]
  FROM [dbo].[AVIONES]

end
GO
/****** Object:  StoredProcedure [dbo].[ListarBoletos]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ListarBoletos]
as
begin
SELECT [id]
      ,[idPasajero]
      ,[idDestino]
      ,[idOrigen]
      ,[precio]
      ,[salida]
      ,[llegada]
      ,[idVuelo]
	  ,[estado]
  FROM [dbo].[BOLETOS]
end
GO
/****** Object:  StoredProcedure [dbo].[ListarDestinos]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ListarDestinos]
as
begin
SELECT [id]
      ,[pais]
      ,[ciudad]
      ,[coordSur]
      ,[coordNorte]
      ,[estado]
  FROM [dbo].[DESTINOS]
  end
GO
/****** Object:  StoredProcedure [dbo].[ListarEscalas]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ListarEscalas]
as
begin
SELECT [id]
      ,[idDestino]
      ,[tLlegada]
      ,[tPartida]
      ,[idVuelo]
  FROM [dbo].[ESCALAS]
end
GO
/****** Object:  StoredProcedure [dbo].[ListarPasajeros]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[ListarPasajeros]
as
begin
SELECT [id]
      ,[nombre]
      ,[apellido]
      ,[fechaNacimiento]
      ,[numeroPasaporte]
      ,[estado]
  FROM [dbo].[PASAJEROS]
end


GO
/****** Object:  StoredProcedure [dbo].[ListarVuelos]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ListarVuelos]
as
begin
	SELECT [id]
      ,[estado]
      ,[tEmbarco]
      ,[tLlegada]
      ,[idAvion]
      ,[tipo]
      ,[idCiudadOrigen]
      ,[idCiudadDestino]
  FROM [dbo].[VUELOS]
end
GO
/****** Object:  StoredProcedure [dbo].[ModificarDestino]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ModificarDestino]
@id int,
@pais varchar(50),
@ciudad varchar(50),
@coordSur float,
@coordNorte float,
@estado int
as
begin
	UPDATE [dbo].[DESTINOS]
	SET [pais] = @pais,
        [ciudad] = @ciudad,
        [coordSur] = @coordSur,
        [coordNorte] = @coordNorte,
        [estado] = @estado
	WHERE id = @id

end
GO
/****** Object:  StoredProcedure [dbo].[ModificarPasajero]    Script Date: 11/17/2020 11:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ModificarPasajero]
@id int,
@nombre varchar(50),
@apellido varchar(50),
@fechaNacimiento date,
@numeroPasaporte numeric(18,0)
as
begin
	UPDATE [dbo].[PASAJEROS]
	SET [nombre] = @nombre,
		[apellido] = @apellido,
		[fechaNacimiento] = @fechaNacimiento,
		[numeroPasaporte] = @numeroPasaporte
	WHERE id = @id
	
end
GO
USE [master]
GO
ALTER DATABASE [AEROPUERTO] SET  READ_WRITE 
GO
