﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_VUELO
    {
        public List<BE.VUELO> Listar()
        {
            MP_AVION mp_avion = new MP_AVION();
            ACCESO acceso = new ACCESO();

            List<BE.AVION> aviones = mp_avion.Listar();
            List<BE.VUELO> vuelos = new List<BE.VUELO>();

            acceso.Abrir();
            DataTable tabla = acceso.EjecutarDT("ListarVuelos");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.AVION avion = (from BE.AVION a in aviones
                                  where a.ID == int.Parse(registro["idAvion"].ToString())
                                  select a).FirstOrDefault();

                BE.VUELO avio = new BE.VUELO(int.Parse(registro["id"].ToString()), avion, registro["tipo"].ToString(), int.Parse(registro["estado"].ToString()), int.Parse(registro["idCiudadOrigen"].ToString()), int.Parse(registro["idCiudadDestino"].ToString()), DateTime.Parse(registro["tLlegada"].ToString()), DateTime.Parse(registro["tEmbarco"].ToString()));
                vuelos.Add(avio);
            }

            return vuelos;
        }

        public Tuple<bool, string> Alta(BE.VUELO vuelo)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("tEmbarco", vuelo.TEmbarco.ToString("yyyy-MM-dd HH:mm")));
            paratros.Add(acceso.CreaParametro("tLlegada", vuelo.TLlegada.ToString("yyyy-MM-dd HH:mm")));
            paratros.Add(acceso.CreaParametro("idAvion", vuelo.Avion.ID));
            paratros.Add(acceso.CreaParametro("tipo", vuelo.Tipo));
            paratros.Add(acceso.CreaParametro("idCiudadOrigen", vuelo.IdCiudadOrigen));
            paratros.Add(acceso.CreaParametro("idCiudadDestino", vuelo.IdCiudadDestino));

            Tuple<bool, string> resultado = acceso.Ejecutar("AltaVuelo", paratros);

            acceso.Cerrar();

            return resultado;
        }

        public Tuple<bool, string> Cancelar(BE.VUELO vuelo)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("id", vuelo.ID));

            Tuple<bool, string> resultado = acceso.Ejecutar("CancelarVuelo", paratros);

            acceso.Cerrar();

            return resultado;
        }
    }
}