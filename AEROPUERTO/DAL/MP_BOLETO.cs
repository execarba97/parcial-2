﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_BOLETO
    {
        public List<BE.BOLETO> Listar()
        {
            ACCESO acceso = new ACCESO();
            MP_ESCALA mp_escala = new MP_ESCALA();
            MP_VUELO mp_vuelo = new MP_VUELO();

            List<BE.BOLETO> ciudades = new List<BE.BOLETO>();
            List<BE.ESCALA> escalas = mp_escala.Listar();
            List<BE.VUELO> vuelos = mp_vuelo.Listar();

            acceso.Abrir();
            DataTable tabla = acceso.EjecutarDT("ListarBoletos");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.VUELO vuelo = (from BE.VUELO v in vuelos
                                  where v.ID == int.Parse(registro["idVuelo"].ToString())
                                  select v).FirstOrDefault();

                BE.BOLETO ciudad = new BE.BOLETO(int.Parse(registro["id"].ToString()), int.Parse(registro["idPasajero"].ToString()), int.Parse(registro["idOrigen"].ToString()), int.Parse(registro["idDestino"].ToString()), float.Parse(registro["precio"].ToString()), DateTime.Parse(registro["salida"].ToString()), DateTime.Parse(registro["llegada"].ToString()), vuelo, int.Parse(registro["estado"].ToString()));
                ciudades.Add(ciudad);
            }

            return ciudades;
        }

        public Tuple<bool, string> Alta(BE.BOLETO ciudad)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("idPasajero", ciudad.IDPasajero));
            paratros.Add(acceso.CreaParametro("idDestino", ciudad.IDDestino));
            paratros.Add(acceso.CreaParametro("idOrigen", ciudad.IDOrigen));
            paratros.Add(acceso.CreaParametro("precio", ciudad.Precio));
            paratros.Add(acceso.CreaParametro("salida", ciudad.HoraSalida));
            paratros.Add(acceso.CreaParametro("llegada", ciudad.HoraLlegada));
            paratros.Add(acceso.CreaParametro("idvuelo", ciudad.Vuelo.ID));

            Tuple<bool, string> resultado = acceso.Ejecutar("AltaBoleto", paratros);

            acceso.Cerrar();

            return resultado;
        }
    }
}