﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_CIUDAD
    {
        public List<BE.CIUDAD> Listar()
        {
            ACCESO acceso = new ACCESO();

            List<BE.CIUDAD> ciudades = new List<BE.CIUDAD>();

            acceso.Abrir();
            DataTable tabla = acceso.EjecutarDT("ListarDestinos");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.CIUDAD ciudad = new BE.CIUDAD(int.Parse(registro["id"].ToString()), registro["pais"].ToString(), registro["ciudad"].ToString(), registro["coordNorte"].ToString(), registro["coordSur"].ToString(), int.Parse(registro["estado"].ToString()));
                ciudades.Add(ciudad);
            }

            return ciudades;
        }

        public Tuple<bool, string> Alta(BE.CIUDAD ciudad)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("pais", ciudad.Pais));
            paratros.Add(acceso.CreaParametro("ciudad", ciudad.Ciudad));
            paratros.Add(acceso.CreaParametro("coordSur", ciudad.CoordSur));
            paratros.Add(acceso.CreaParametro("coordNorte", ciudad.CoordNorte));

            Tuple<bool, string> resultado = acceso.Ejecutar("AltaDestino", paratros);

            acceso.Cerrar();

            return resultado;
        }

        public Tuple<bool, string> Modificar(BE.CIUDAD ciudad)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("id", ciudad.ID));
            paratros.Add(acceso.CreaParametro("pais", ciudad.Pais));
            paratros.Add(acceso.CreaParametro("ciudad", ciudad.Ciudad));
            paratros.Add(acceso.CreaParametro("coordSur", ciudad.CoordSur));
            paratros.Add(acceso.CreaParametro("coordNorte", ciudad.CoordNorte));
            paratros.Add(acceso.CreaParametro("estado", ciudad.Estado));

            Tuple<bool, string> resultado = acceso.Ejecutar("ModificarDestino", paratros);

            acceso.Cerrar();

            return resultado;
        }

        public Tuple<bool, string> Baja(BE.CIUDAD ciudad)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("id", ciudad.ID));

            Tuple<bool, string> resultado = acceso.Ejecutar("BajaDestino", paratros);

            acceso.Cerrar();

            return resultado;
        }
    }
}