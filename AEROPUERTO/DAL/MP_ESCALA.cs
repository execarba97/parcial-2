﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_ESCALA
    {
        public List<BE.ESCALA> Listar()
        {
            ACCESO acceso = new ACCESO();

            List<BE.ESCALA> ciudades = new List<BE.ESCALA>();

            acceso.Abrir();
            DataTable tabla = acceso.EjecutarDT("ListarEscalas");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.ESCALA ciudad = new BE.ESCALA(int.Parse(registro["id"].ToString()), int.Parse(registro["idDestino"].ToString()), DateTime.Parse(registro["tLlegada"].ToString()), DateTime.Parse(registro["tPartida"].ToString()), int.Parse(registro["idVuelo"].ToString()));
                ciudades.Add(ciudad);
            }

            return ciudades;
        }

        public Tuple<bool, string> Alta(BE.ESCALA escala)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("idDestino", escala.IDDestino));
            paratros.Add(acceso.CreaParametro("tLlegada", escala.TLlegada.ToString("yyyy-MM-dd HH:mm")));
            paratros.Add(acceso.CreaParametro("tPartida", escala.TPartida.ToString("yyyy-MM-dd HH:mm")));
            paratros.Add(acceso.CreaParametro("idVuelo", escala.IDVuelo));

            Tuple<bool, string> resultado = acceso.Ejecutar("AltaEscala", paratros);

            acceso.Cerrar();

            return resultado;
        }
    }
}