﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class ACCESO
    {
        private SqlConnection conexion;

        public void Abrir()
        {
            conexion = new SqlConnection();
            conexion.ConnectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=AEROPUERTO;Integrated Security=SSPI";
            conexion.Open();
        }

        public void Cerrar()
        {
            conexion.Close();
            conexion = null;
            GC.Collect();
        }

        private SqlCommand CreaComando(string sql, List<SqlParameter> parametros = null, CommandType tipo = CommandType.Text)
        {
            SqlCommand comando = new SqlCommand(sql);
            comando.CommandType = tipo;

            if (parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());

            }
            comando.Connection = conexion;

            return comando;
        }

        public SqlParameter CreaParametro(string nombre, string valor)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);

            parametro.DbType = DbType.String;

            return parametro;
        }

        public SqlParameter CreaParametro(string nombre, int valor)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);

            parametro.DbType = DbType.String;

            return parametro;
        }

        public SqlParameter CreaParametro(string nombre, float valor)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);

            parametro.DbType = DbType.String;

            return parametro;
        }

        public SqlParameter CreaParametro(string nombre, Int64 valor)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);

            parametro.DbType = DbType.Int64;

            return parametro;
        }

        public SqlParameter CreaParametro(string nombre, DateTime valor)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);

            parametro.DbType = DbType.DateTime;

            return parametro;
        }
        public Tuple<bool, string> Ejecutar(string sql, List<SqlParameter> parametros = null)
        {
            SqlCommand comando = CreaComando(sql, parametros, CommandType.StoredProcedure);
            comando.Connection = conexion;
            int filasAfectadas = 0;

            try
            {
                filasAfectadas = comando.ExecuteNonQuery();
                return new Tuple<bool, string>(true, "Cantidad de filas afectadas " + filasAfectadas.ToString());
            }
            catch (SqlException ex)
            {
                return new Tuple<bool, string>(false, "Error de sql: " + ex.ToString());
            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, "Error generico: " + ex.ToString());
            }
        }

        public DataTable EjecutarDT(string sql, List<SqlParameter> parametros = null)
        {
            SqlDataAdapter adaptador = new SqlDataAdapter();

            adaptador.SelectCommand = CreaComando(sql, parametros);

            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);

            return tabla;
        }
    }
}
