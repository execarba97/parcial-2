﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_AVION
    {
        public List<BE.AVION> Listar()
        {
            List<BE.AVION> aviones = new List<BE.AVION>();
            ACCESO acceso = new ACCESO();

            acceso.Abrir();
            DataTable tabla = acceso.EjecutarDT("ListarAviones");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.AVION emp = new BE.AVION(int.Parse(registro["id"].ToString()), int.Parse(registro["capacidad"].ToString()), registro["marca"].ToString(), registro["aerolinea"].ToString(), registro["patente"].ToString());
                aviones.Add(emp);
            }

            return aviones;
        }

        public Tuple<bool, string> Alta(BE.AVION avion)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("aerolinea", avion.Aerolinea));
            paratros.Add(acceso.CreaParametro("capacidad", avion.Capacidad));
            paratros.Add(acceso.CreaParametro("patente", avion.Patente));
            paratros.Add(acceso.CreaParametro("marca", avion.Marca));

            Tuple<bool,string> resultado = acceso.Ejecutar("AltaAvion", paratros);

            acceso.Cerrar();

            return resultado;
        }
    }
}