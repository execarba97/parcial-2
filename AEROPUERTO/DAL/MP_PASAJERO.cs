﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_PASAJERO
    {
        public List<BE.PASAJERO> Listar()
        {
            ACCESO acceso = new ACCESO();

            List<BE.PASAJERO> pasajeros = new List<BE.PASAJERO>();

            acceso.Abrir();
            DataTable tabla = acceso.EjecutarDT("ListarPasajeros");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.PASAJERO pasajero = new BE.PASAJERO(int.Parse(registro["id"].ToString()), registro["nombre"].ToString(), registro["apellido"].ToString(), registro["numeroPasaporte"].ToString(), DateTime.Parse(registro["fechaNacimiento"].ToString()), int.Parse(registro["estado"].ToString()));
                pasajeros.Add(pasajero);
            }

            return pasajeros;
        }

        public Tuple<bool, string> Alta(BE.PASAJERO pasajero)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("nombre", pasajero.Nombre));
            paratros.Add(acceso.CreaParametro("apellido", pasajero.Apellido));
            paratros.Add(acceso.CreaParametro("fechaNacimiento", pasajero.FechaNacimiento));
            paratros.Add(acceso.CreaParametro("numeroPasaporte", pasajero.Pasaporte));

            Tuple<bool, string> resultado = acceso.Ejecutar("AltaPasajero", paratros);

            acceso.Cerrar();

            return resultado;
        }

        public Tuple<bool, string> Modificar(BE.PASAJERO pasajero)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("id", pasajero.ID));
            paratros.Add(acceso.CreaParametro("nombre", pasajero.Nombre));
            paratros.Add(acceso.CreaParametro("apellido", pasajero.Apellido));
            paratros.Add(acceso.CreaParametro("fechaNacimiento", pasajero.FechaNacimiento));
            paratros.Add(acceso.CreaParametro("numeroPasaporte", pasajero.Pasaporte));

            Tuple<bool, string> resultado = acceso.Ejecutar("ModificarPasajero", paratros);

            acceso.Cerrar();

            return resultado;
        }

        public Tuple<bool, string> Baja(BE.PASAJERO pasajero)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("id", pasajero.ID));

            Tuple<bool, string> resultado = acceso.Ejecutar("BajaPasajero", paratros);

            acceso.Cerrar();

            return resultado;
        }
    }
}