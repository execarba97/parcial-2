﻿namespace AEROPUERTO
{
    partial class frmAltaEscalas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbCiudades = new System.Windows.Forms.ComboBox();
            this.dtLlegada = new System.Windows.Forms.DateTimePicker();
            this.dtPartida = new System.Windows.Forms.DateTimePicker();
            this.btnAgregarEscala = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ciudad";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Llegada";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "Partida";
            // 
            // cbCiudades
            // 
            this.cbCiudades.FormattingEnabled = true;
            this.cbCiudades.Location = new System.Drawing.Point(83, 6);
            this.cbCiudades.Name = "cbCiudades";
            this.cbCiudades.Size = new System.Drawing.Size(200, 24);
            this.cbCiudades.TabIndex = 3;
            // 
            // dtLlegada
            // 
            this.dtLlegada.Location = new System.Drawing.Point(83, 37);
            this.dtLlegada.Name = "dtLlegada";
            this.dtLlegada.Size = new System.Drawing.Size(200, 22);
            this.dtLlegada.TabIndex = 4;
            this.dtLlegada.ValueChanged += new System.EventHandler(this.dtLlegada_ValueChanged);
            // 
            // dtPartida
            // 
            this.dtPartida.Location = new System.Drawing.Point(83, 65);
            this.dtPartida.Name = "dtPartida";
            this.dtPartida.Size = new System.Drawing.Size(200, 22);
            this.dtPartida.TabIndex = 5;
            // 
            // btnAgregarEscala
            // 
            this.btnAgregarEscala.Location = new System.Drawing.Point(289, 6);
            this.btnAgregarEscala.Name = "btnAgregarEscala";
            this.btnAgregarEscala.Size = new System.Drawing.Size(102, 81);
            this.btnAgregarEscala.TabIndex = 6;
            this.btnAgregarEscala.Text = "Agregar escala";
            this.btnAgregarEscala.UseVisualStyleBackColor = true;
            this.btnAgregarEscala.Click += new System.EventHandler(this.btnAgregarEscala_Click);
            // 
            // frmAltaEscalas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 124);
            this.Controls.Add(this.btnAgregarEscala);
            this.Controls.Add(this.dtPartida);
            this.Controls.Add(this.dtLlegada);
            this.Controls.Add(this.cbCiudades);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmAltaEscalas";
            this.Text = "frmAltaEscalas";
            this.Load += new System.EventHandler(this.frmAltaEscalas_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbCiudades;
        private System.Windows.Forms.DateTimePicker dtLlegada;
        private System.Windows.Forms.DateTimePicker dtPartida;
        private System.Windows.Forms.Button btnAgregarEscala;
    }
}