﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void nuevoDestinoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDestinosAlta frm = new frmDestinosAlta();
            frm.MdiParent = this;
            frm.Show();
        }

        private void altaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmVueloAlta frm = new frmVueloAlta();
            frm.MdiParent = this;
            frm.Show();
        }

        private void cancelacionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmVueloCancelar frm = new frmVueloCancelar();
            frm.MdiParent = this;
            frm.Show();
        }

        private void eliminarDestinoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDestinosBaja frm = new frmDestinosBaja();
            frm.MdiParent = this;
            frm.Show();
        }

        private void modificarDestinoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDestinoModificar frm = new frmDestinoModificar();
            frm.MdiParent = this;
            frm.Show();
        }

        private void altaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmPasajerosAlta frm = new frmPasajerosAlta();
            frm.MdiParent = this;
            frm.Show();
        }

        private void bajaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPasajerosBaja frm = new frmPasajerosBaja();
            frm.MdiParent = this;
            frm.Show();
        }

        private void modificacionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPasajeroModificar frm = new frmPasajeroModificar();
            frm.MdiParent = this;
            frm.Show();
        }

        private void nuevoAvionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAltaAvion frm = new frmAltaAvion();
            frm.MdiParent = this;
            frm.Show();
        }

        private void venderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAltaBoleto frm = new frmAltaBoleto();
            frm.MdiParent = this;
            frm.Show();
        }

        private void consultasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmVueloConsultar frm = new frmVueloConsultar();
            frm.MdiParent = this;
            frm.Show();
        }

        private void visualizadorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDestinoVisualizar frm = new frmDestinoVisualizar();
            frm.MdiParent = this;
            frm.Show();
        }
    }
}
