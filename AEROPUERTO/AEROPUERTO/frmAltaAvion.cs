﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class frmAltaAvion : Form
    {
        public frmAltaAvion()
        {
            InitializeComponent();
        }

        private void frmAltaAvion_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if(VALIDADOR.ValidaText(txtCapacidad.Text) && VALIDADOR.ValidaText(txtAerolinea.Text) && VALIDADOR.ValidaText(txtPatente.Text) && VALIDADOR.ValidarNumero(txtCapacidad.Text))
            {
                BE.AVION avion = new BE.AVION(0, int.Parse(txtCapacidad.Text), txtMarca.Text, txtAerolinea.Text, txtPatente.Text);
                
                Tuple<bool, string> resultado = BLL.AVION.Escribir(avion);
                if (resultado.Item1)
                {
                    Enlazar();
                }
                else
                {
                    MessageBox.Show(resultado.Item2);
                }
            }
        }

        void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = BLL.AVION.Listar();
        }
    }
}
