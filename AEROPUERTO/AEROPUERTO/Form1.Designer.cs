﻿namespace AEROPUERTO
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.vuelosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.altaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelacionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoAvionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasajerosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.altaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bajaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificacionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.destinosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoDestinoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarDestinoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarDestinoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.boletosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.venderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visualizadorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vuelosToolStripMenuItem,
            this.pasajerosToolStripMenuItem,
            this.destinosToolStripMenuItem,
            this.boletosToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // vuelosToolStripMenuItem
            // 
            this.vuelosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.altaToolStripMenuItem,
            this.consultasToolStripMenuItem,
            this.cancelacionToolStripMenuItem,
            this.nuevoAvionToolStripMenuItem});
            this.vuelosToolStripMenuItem.Name = "vuelosToolStripMenuItem";
            this.vuelosToolStripMenuItem.Size = new System.Drawing.Size(67, 24);
            this.vuelosToolStripMenuItem.Text = "Vuelos";
            // 
            // altaToolStripMenuItem
            // 
            this.altaToolStripMenuItem.Name = "altaToolStripMenuItem";
            this.altaToolStripMenuItem.Size = new System.Drawing.Size(177, 26);
            this.altaToolStripMenuItem.Text = "Alta";
            this.altaToolStripMenuItem.Click += new System.EventHandler(this.altaToolStripMenuItem_Click);
            // 
            // consultasToolStripMenuItem
            // 
            this.consultasToolStripMenuItem.Name = "consultasToolStripMenuItem";
            this.consultasToolStripMenuItem.Size = new System.Drawing.Size(177, 26);
            this.consultasToolStripMenuItem.Text = "Consultas";
            this.consultasToolStripMenuItem.Click += new System.EventHandler(this.consultasToolStripMenuItem_Click);
            // 
            // cancelacionToolStripMenuItem
            // 
            this.cancelacionToolStripMenuItem.Name = "cancelacionToolStripMenuItem";
            this.cancelacionToolStripMenuItem.Size = new System.Drawing.Size(177, 26);
            this.cancelacionToolStripMenuItem.Text = "Cancelacion";
            this.cancelacionToolStripMenuItem.Click += new System.EventHandler(this.cancelacionToolStripMenuItem_Click);
            // 
            // nuevoAvionToolStripMenuItem
            // 
            this.nuevoAvionToolStripMenuItem.Name = "nuevoAvionToolStripMenuItem";
            this.nuevoAvionToolStripMenuItem.Size = new System.Drawing.Size(177, 26);
            this.nuevoAvionToolStripMenuItem.Text = "Nuevo Avion";
            this.nuevoAvionToolStripMenuItem.Click += new System.EventHandler(this.nuevoAvionToolStripMenuItem_Click);
            // 
            // pasajerosToolStripMenuItem
            // 
            this.pasajerosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.altaToolStripMenuItem1,
            this.bajaToolStripMenuItem,
            this.modificacionToolStripMenuItem});
            this.pasajerosToolStripMenuItem.Name = "pasajerosToolStripMenuItem";
            this.pasajerosToolStripMenuItem.Size = new System.Drawing.Size(84, 24);
            this.pasajerosToolStripMenuItem.Text = "Pasajeros";
            // 
            // altaToolStripMenuItem1
            // 
            this.altaToolStripMenuItem1.Name = "altaToolStripMenuItem1";
            this.altaToolStripMenuItem1.Size = new System.Drawing.Size(179, 26);
            this.altaToolStripMenuItem1.Text = "Alta";
            this.altaToolStripMenuItem1.Click += new System.EventHandler(this.altaToolStripMenuItem1_Click);
            // 
            // bajaToolStripMenuItem
            // 
            this.bajaToolStripMenuItem.Name = "bajaToolStripMenuItem";
            this.bajaToolStripMenuItem.Size = new System.Drawing.Size(179, 26);
            this.bajaToolStripMenuItem.Text = "Baja";
            this.bajaToolStripMenuItem.Click += new System.EventHandler(this.bajaToolStripMenuItem_Click);
            // 
            // modificacionToolStripMenuItem
            // 
            this.modificacionToolStripMenuItem.Name = "modificacionToolStripMenuItem";
            this.modificacionToolStripMenuItem.Size = new System.Drawing.Size(179, 26);
            this.modificacionToolStripMenuItem.Text = "Modificacion";
            this.modificacionToolStripMenuItem.Click += new System.EventHandler(this.modificacionToolStripMenuItem_Click);
            // 
            // destinosToolStripMenuItem
            // 
            this.destinosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoDestinoToolStripMenuItem,
            this.modificarDestinoToolStripMenuItem,
            this.eliminarDestinoToolStripMenuItem,
            this.visualizadorToolStripMenuItem});
            this.destinosToolStripMenuItem.Name = "destinosToolStripMenuItem";
            this.destinosToolStripMenuItem.Size = new System.Drawing.Size(80, 24);
            this.destinosToolStripMenuItem.Text = "Destinos";
            // 
            // nuevoDestinoToolStripMenuItem
            // 
            this.nuevoDestinoToolStripMenuItem.Name = "nuevoDestinoToolStripMenuItem";
            this.nuevoDestinoToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.nuevoDestinoToolStripMenuItem.Text = "Nuevo destino";
            this.nuevoDestinoToolStripMenuItem.Click += new System.EventHandler(this.nuevoDestinoToolStripMenuItem_Click);
            // 
            // modificarDestinoToolStripMenuItem
            // 
            this.modificarDestinoToolStripMenuItem.Name = "modificarDestinoToolStripMenuItem";
            this.modificarDestinoToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.modificarDestinoToolStripMenuItem.Text = "Modificar destino";
            this.modificarDestinoToolStripMenuItem.Click += new System.EventHandler(this.modificarDestinoToolStripMenuItem_Click);
            // 
            // eliminarDestinoToolStripMenuItem
            // 
            this.eliminarDestinoToolStripMenuItem.Name = "eliminarDestinoToolStripMenuItem";
            this.eliminarDestinoToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.eliminarDestinoToolStripMenuItem.Text = "Eliminar destino";
            this.eliminarDestinoToolStripMenuItem.Click += new System.EventHandler(this.eliminarDestinoToolStripMenuItem_Click);
            // 
            // boletosToolStripMenuItem
            // 
            this.boletosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.venderToolStripMenuItem});
            this.boletosToolStripMenuItem.Name = "boletosToolStripMenuItem";
            this.boletosToolStripMenuItem.Size = new System.Drawing.Size(73, 24);
            this.boletosToolStripMenuItem.Text = "Boletos";
            // 
            // venderToolStripMenuItem
            // 
            this.venderToolStripMenuItem.Name = "venderToolStripMenuItem";
            this.venderToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.venderToolStripMenuItem.Text = "Vender";
            this.venderToolStripMenuItem.Click += new System.EventHandler(this.venderToolStripMenuItem_Click);
            // 
            // visualizadorToolStripMenuItem
            // 
            this.visualizadorToolStripMenuItem.Name = "visualizadorToolStripMenuItem";
            this.visualizadorToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.visualizadorToolStripMenuItem.Text = "Visualizador";
            this.visualizadorToolStripMenuItem.Click += new System.EventHandler(this.visualizadorToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem vuelosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem altaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelacionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasajerosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem altaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem bajaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificacionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem destinosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoDestinoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarDestinoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarDestinoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoAvionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem boletosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem venderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem visualizadorToolStripMenuItem;
    }
}

