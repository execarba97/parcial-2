﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class frmDestinosAlta : Form
    {
        public frmDestinosAlta()
        {
            InitializeComponent();
        }

        private void frmDestinosAlta_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = (from BE.CIUDAD c in BLL.CIUDAD.Listar()
                                        where c.Estado == 1
                                        select c).ToList();
        }
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if(VALIDADOR.ValidaText(txtCiudad.Text) && VALIDADOR.ValidaText(txtPais.Text) && VALIDADOR.ValidaText(txtCS.Text) && VALIDADOR.ValidaText(txtCN.Text))
            {
                BE.CIUDAD ciudad = new BE.CIUDAD(0, txtPais.Text, txtCiudad.Text, txtCN.Text, txtCS.Text, 1);
                
                Tuple<bool, string> resultado = BLL.CIUDAD.Escribir(ciudad);
                if (resultado.Item1)
                {
                    Enlazar();
                }
                else
                {
                    MessageBox.Show(resultado.Item2);
                }
            }
        }
    }
}
