﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class frmPasajeroModificar : Form
    {
        public frmPasajeroModificar()
        {
            InitializeComponent();
        }

        void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = (from BE.PASAJERO p in BLL.PASAJERO.Listar()
                                        where p.Estado == 1
                                        select p).ToList();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            BE.PASAJERO pasajero = (BE.PASAJERO)dataGridView1.CurrentRow.DataBoundItem;

            txtNombre.Text = pasajero.Nombre;
            txtApellido.Text = pasajero.Apellido;
            txtPasaporte.Text = pasajero.Pasaporte;
            dtFechaNacimiento.Value = pasajero.FechaNacimiento;
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if(VALIDADOR.ValidaText(txtApellido.Text) && VALIDADOR.ValidaText(txtPasaporte.Text) && VALIDADOR.ValidaText(txtNombre.Text) && dtFechaNacimiento.Value < DateTime.Now)
            {
                BE.PASAJERO pasajeroAux = (BE.PASAJERO)dataGridView1.CurrentRow.DataBoundItem;
                BE.PASAJERO pasajero = new BE.PASAJERO(pasajeroAux.ID, txtNombre.Text, txtApellido.Text, txtPasaporte.Text, dtFechaNacimiento.Value, 1);
                Tuple<bool, string> resultado = BLL.PASAJERO.Modificar(pasajero);
                if (resultado.Item1)
                {
                    Enlazar();
                }
                else
                {
                    MessageBox.Show(resultado.Item2);
                }
            }
            else
            {
                MessageBox.Show("Error al ingresar los campos.");
            }
        }

        private void frmPasajeroModificar_Load(object sender, EventArgs e)
        {
            Enlazar();
        }
    }
}
