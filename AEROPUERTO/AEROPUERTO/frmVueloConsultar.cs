﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class frmVueloConsultar : Form
    {
        public frmVueloConsultar()
        {
            InitializeComponent();
        }

        List<BE.EnumTipoVuelo> tipos = new List<BE.EnumTipoVuelo>();
        

        void EnlazarFiltros()
        {
            cbDestino.DataSource = null;
            cbDestino.DataSource = (from BE.CIUDAD c in BLL.CIUDAD.Listar()
                                    select c).ToList();

            cbTipo.DataSource = null;
            cbTipo.DataSource = tipos;
        }

        private void frmVueloConsultar_Load(object sender, EventArgs e)
        {
            tipos.Clear();
            tipos.Add(BE.EnumTipoVuelo.Internacional);
            tipos.Add(BE.EnumTipoVuelo.Nacional);

            EnlazarFiltros();
        }

        void BuscarVuelos()
        {
            BE.CIUDAD ciudad = (BE.CIUDAD)cbDestino.SelectedItem;
            BE.EnumTipoVuelo tipo = (BE.EnumTipoVuelo)cbTipo.SelectedItem;
            DateTime desde = dtDesde.Value;
            DateTime hasta = dtHasta.Value;

            List<BE.VUELO> vuelos = (from BE.VUELO v in BLL.VUELO.Listar()
                                     where v.IdCiudadDestino == ciudad.ID && v.Tipo == tipo.ToString() && v.TEmbarco >= desde && v.TLlegada <= hasta
                                     select v).ToList();

            if(vuelos.Count() > 0)
            {
                dtgVuelos.DataSource = null;
                dtgVuelos.DataSource = vuelos;
            }
            else
            {
                MessageBox.Show("No hay vuelos.");
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            BuscarVuelos();
        }

        private void dtgVuelos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            BE.VUELO vuelo = (BE.VUELO)dtgVuelos.CurrentRow.DataBoundItem;

            lblAvion.Text = (vuelo.Avion).ToString();
            lblDestino.Text = (BLL.CIUDAD.BuscarCiudad(vuelo.IdCiudadDestino)).ToString();
            lblOrigen.Text = (BLL.CIUDAD.BuscarCiudad(vuelo.IdCiudadOrigen)).ToString();
            lblEmbarco.Text = vuelo.TEmbarco.ToString("dd-MM-yyyy HH:mm");
            lblLlegada.Text = vuelo.TLlegada.ToString("dd-MM-yyyy HH:mm");
            lblEscalas.Text = (from BE.ESCALA esc in BLL.ESCALA.Listar()
                               where esc.IDVuelo == vuelo.ID
                               select esc).ToList().Count().ToString();

            List<BE.BOLETO> boletos = (from BE.BOLETO b in BLL.BOLETO.Listar()
                                       where b.Vuelo.ID == vuelo.ID
                                       select b).ToList();

            List<BE.PASAJERO> pasajeros = new List<BE.PASAJERO>();
            foreach(BE.BOLETO b in boletos)
            {
                pasajeros.Add((from BE.PASAJERO p in BLL.PASAJERO.Listar()
                               where p.ID == b.IDPasajero
                               select p).FirstOrDefault());
            }

            if(pasajeros != null)
            {
                lblOcupacion.Text = (pasajeros.Count() * 100 / vuelo.Avion.Capacidad).ToString() + "%";
                dtgPasajeros.DataSource = null;
                dtgPasajeros.DataSource = pasajeros;
            }
        }

        private void dtgPasajeros_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            BE.PASAJERO pasajero = (BE.PASAJERO)dtgPasajeros.CurrentRow.DataBoundItem;
            lblNombre.Text = pasajero.Nombre;
            lblApellido.Text = pasajero.Apellido;
            lblPasaporte.Text = pasajero.Pasaporte;
            lblEdad.Text = (DateTime.Now.Year - pasajero.FechaNacimiento.Year).ToString();        
        }
    }
}
