﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class frmAltaBoleto : Form
    {
        int flag = 1;
        public frmAltaBoleto()
        {
            InitializeComponent();
        }

        void Enlazar()
        {
            cbDestino.DataSource = null;
            cbDestino.DataSource = (from BE.CIUDAD c in BLL.CIUDAD.Listar()
                                    where c.Estado == 1
                                    select c).ToList();

            cbOrigen.DataSource = null;
            cbOrigen.DataSource = (from BE.CIUDAD c in BLL.CIUDAD.Listar()
                                   where c.Estado == 1
                                   select c).ToList();

            cbPasajero.DataSource = null;
            cbPasajero.DataSource = (from BE.PASAJERO p in BLL.PASAJERO.Listar()
                                     where p.Estado == 1
                                     select p).ToList();

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = BLL.BOLETO.Listar();
        }

        void BuscarVuelo()
        {
            List<BE.VUELO> vuelos = new List<BE.VUELO>();
            try
            {
                vuelos = (from BE.VUELO v in BLL.VUELO.Listar()
                          where v.IdCiudadOrigen == ((BE.CIUDAD)cbOrigen.SelectedItem).ID && v.IdCiudadDestino == ((BE.CIUDAD)cbDestino.SelectedItem).ID && v.Avion.Capacidad > (from BE.BOLETO b in BLL.BOLETO.Listar() where b.Vuelo.ID == v.ID && b.Estado == 1 select b).Count() && v.Estado == 1
                          select v).ToList();
            }
            catch
            {
                vuelos = null;
            }

            if (vuelos.Count() > 0)
            {
                cbVuelos.DataSource = null;
                cbVuelos.DataSource = vuelos;
            }
            else
            {
                MessageBox.Show("Sin vuelos disponibles para la combinacion de Origen/Destino seleccionada.");
            }
        }

        private void frmAltaBoleto_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void cbOrigen_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void cbDestino_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void cbVuelos_SelectedIndexChanged(object sender, EventArgs e)
        {
            BE.VUELO vuelo = (BE.VUELO)cbVuelos.SelectedItem;

            if(vuelo != null)
            {
                lblLlegada.Text = vuelo.TLlegada.ToString("dd-MM-yyyy HH:mm");
                lblSalida.Text = vuelo.TEmbarco.ToString("dd-MM-yyyy HH:mm");
                lblEscalas.Text = "Cantidad de escalas: " + (from BE.ESCALA esc in BLL.ESCALA.Listar()
                                                             where esc.IDVuelo == vuelo.ID
                                                             select esc).ToList().Count().ToString();
            }
            
        }

        void LimpiarControles()
        {
            lblEscalas.Text = "";
            lblLlegada.Text = "";
            lblSalida.Text = "";
            txtPrecio.Text = "";
            cbVuelos.DataSource = null;
        }

        void BorrarBoleto()
        {
            foreach(Control c in this.Controls)
            {
                if(c is controlBoleto)
                {
                    c.Dispose();
                }
            }
        }

        bool ValidarPasajero()
        {
            BE.PASAJERO pasajero = (BE.PASAJERO)cbPasajero.SelectedItem;
            BE.VUELO vuelo = (BE.VUELO)cbVuelos.SelectedItem;
            BE.BOLETO boleto = (from BE.BOLETO b in BLL.BOLETO.Listar()
                                where pasajero.ID == b.IDPasajero && b.Vuelo.ID == vuelo.ID && b.Vuelo.TEmbarco == vuelo.TEmbarco && b.Vuelo.TLlegada == vuelo.TLlegada
                                select b).FirstOrDefault();
            if(boleto == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void btnComprar_Click(object sender, EventArgs e)
        {
            if(cbVuelos.SelectedItem != null && ValidarPasajero() && VALIDADOR.ValidaFloat(txtPrecio.Text))
            {
                BorrarBoleto();
                BE.VUELO vuelo = (BE.VUELO)cbVuelos.SelectedItem;
                BE.BOLETO boleto = new BE.BOLETO(0, ((BE.PASAJERO)cbPasajero.SelectedItem).ID, ((BE.CIUDAD)cbOrigen.SelectedItem).ID, ((BE.CIUDAD)cbDestino.SelectedItem).ID, float.Parse(txtPrecio.Text), vuelo.TEmbarco, vuelo.TLlegada, vuelo, 1);

                Tuple<bool, string> resultado = BLL.BOLETO.Escribir(boleto);
                if (resultado.Item1)
                {
                    controlBoleto ticket = new controlBoleto();
                    ticket.ObtenerBoleto(boleto);
                    this.Controls.Add(ticket);
                    ticket.Location = new Point(12, 371);
                    Enlazar();

                    LimpiarControles();
                }
                else
                {
                    MessageBox.Show(resultado.Item2);
                }
            }
            else if(cbVuelos.SelectedItem == null)
            {
                MessageBox.Show("Debe seleccionar un vuelo antes de comprar.");
            }
            else
            {
                MessageBox.Show("El pasajero ya tiene un pasaje para este vuelo.");
            }
        }

        private void btnBuscarVuelo_Click(object sender, EventArgs e)
        {
            if (cbDestino.SelectedItem != null && cbOrigen.SelectedItem != null)
            {
                BuscarVuelo();
            }
        }
    }
}
