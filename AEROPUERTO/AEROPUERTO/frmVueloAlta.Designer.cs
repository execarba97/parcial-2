﻿namespace AEROPUERTO
{
    partial class frmVueloAlta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtgVuelos = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dtEmbarco = new System.Windows.Forms.DateTimePicker();
            this.dtLlegada = new System.Windows.Forms.DateTimePicker();
            this.cbAvion = new System.Windows.Forms.ComboBox();
            this.cbOrigen = new System.Windows.Forms.ComboBox();
            this.cbDestino = new System.Windows.Forms.ComboBox();
            this.btnAlta = new System.Windows.Forms.Button();
            this.btnAgregarEscala = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtgVuelos)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgVuelos
            // 
            this.dtgVuelos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgVuelos.Location = new System.Drawing.Point(12, 130);
            this.dtgVuelos.Name = "dtgVuelos";
            this.dtgVuelos.RowHeadersWidth = 51;
            this.dtgVuelos.RowTemplate.Height = 24;
            this.dtgVuelos.Size = new System.Drawing.Size(1396, 296);
            this.dtgVuelos.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Embarco";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Llegada";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Avion";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(340, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Origen";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(340, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Destino";
            // 
            // dtEmbarco
            // 
            this.dtEmbarco.Location = new System.Drawing.Point(83, 19);
            this.dtEmbarco.MinDate = new System.DateTime(2020, 1, 25, 0, 0, 0, 0);
            this.dtEmbarco.Name = "dtEmbarco";
            this.dtEmbarco.Size = new System.Drawing.Size(241, 22);
            this.dtEmbarco.TabIndex = 7;
            this.dtEmbarco.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // dtLlegada
            // 
            this.dtLlegada.Location = new System.Drawing.Point(83, 47);
            this.dtLlegada.MinDate = new System.DateTime(2020, 1, 25, 0, 0, 0, 0);
            this.dtLlegada.Name = "dtLlegada";
            this.dtLlegada.Size = new System.Drawing.Size(241, 22);
            this.dtLlegada.TabIndex = 8;
            // 
            // cbAvion
            // 
            this.cbAvion.FormattingEnabled = true;
            this.cbAvion.Location = new System.Drawing.Point(83, 75);
            this.cbAvion.Name = "cbAvion";
            this.cbAvion.Size = new System.Drawing.Size(241, 24);
            this.cbAvion.TabIndex = 9;
            // 
            // cbOrigen
            // 
            this.cbOrigen.FormattingEnabled = true;
            this.cbOrigen.Location = new System.Drawing.Point(397, 19);
            this.cbOrigen.Name = "cbOrigen";
            this.cbOrigen.Size = new System.Drawing.Size(241, 24);
            this.cbOrigen.TabIndex = 10;
            // 
            // cbDestino
            // 
            this.cbDestino.FormattingEnabled = true;
            this.cbDestino.Location = new System.Drawing.Point(397, 47);
            this.cbDestino.Name = "cbDestino";
            this.cbDestino.Size = new System.Drawing.Size(241, 24);
            this.cbDestino.TabIndex = 11;
            // 
            // btnAlta
            // 
            this.btnAlta.Location = new System.Drawing.Point(644, 19);
            this.btnAlta.Name = "btnAlta";
            this.btnAlta.Size = new System.Drawing.Size(107, 82);
            this.btnAlta.TabIndex = 12;
            this.btnAlta.Text = "Agregar";
            this.btnAlta.UseVisualStyleBackColor = true;
            this.btnAlta.Click += new System.EventHandler(this.btnAlta_Click);
            // 
            // btnAgregarEscala
            // 
            this.btnAgregarEscala.Location = new System.Drawing.Point(397, 75);
            this.btnAgregarEscala.Name = "btnAgregarEscala";
            this.btnAgregarEscala.Size = new System.Drawing.Size(241, 26);
            this.btnAgregarEscala.TabIndex = 13;
            this.btnAgregarEscala.Text = "AgregarEscala";
            this.btnAgregarEscala.UseVisualStyleBackColor = true;
            this.btnAgregarEscala.Click += new System.EventHandler(this.btnAgregarEscala_Click);
            // 
            // frmVueloAlta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1481, 449);
            this.Controls.Add(this.btnAgregarEscala);
            this.Controls.Add(this.btnAlta);
            this.Controls.Add(this.cbDestino);
            this.Controls.Add(this.cbOrigen);
            this.Controls.Add(this.cbAvion);
            this.Controls.Add(this.dtLlegada);
            this.Controls.Add(this.dtEmbarco);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtgVuelos);
            this.Name = "frmVueloAlta";
            this.Text = "frmVueloAlta";
            this.Load += new System.EventHandler(this.frmVueloAlta_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgVuelos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgVuelos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtEmbarco;
        private System.Windows.Forms.DateTimePicker dtLlegada;
        private System.Windows.Forms.ComboBox cbAvion;
        private System.Windows.Forms.ComboBox cbOrigen;
        private System.Windows.Forms.ComboBox cbDestino;
        private System.Windows.Forms.Button btnAlta;
        private System.Windows.Forms.Button btnAgregarEscala;
    }
}