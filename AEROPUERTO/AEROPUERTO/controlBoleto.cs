﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class controlBoleto : UserControl
    {
        public controlBoleto()
        {
            InitializeComponent();
        }
        public void ObtenerBoleto(BE.BOLETO b)
        {
            BE.PASAJERO pasajero = (from BE.PASAJERO p in BLL.PASAJERO.Listar()
                                    where p.ID == b.IDPasajero
                                    select p).FirstOrDefault();
            
            BE.CIUDAD ciudadOrigen = (from BE.CIUDAD c in BLL.CIUDAD.Listar()
                                      where c.ID == b.IDOrigen
                                      select c).FirstOrDefault();

            BE.CIUDAD ciudadDestino = (from BE.CIUDAD c in BLL.CIUDAD.Listar()
                                      where c.ID == b.IDDestino
                                      select c).FirstOrDefault();

            lblnombre.Text = pasajero.Nombre;
            lblApellido.Text = pasajero.Apellido;
            lblEdad.Text = (DateTime.Now.Year - pasajero.FechaNacimiento.Year).ToString();
            lblDesitno.Text = ciudadDestino.Ciudad + ", " + ciudadDestino.Pais;
            lblOrigen.Text = ciudadOrigen.Ciudad + ", " + ciudadOrigen.Pais;
            lblPatente.Text = b.Vuelo.Avion.Patente;
            lblPrecio.Text = b.Precio.ToString();
            lblSalida.Text = b.Vuelo.TEmbarco.ToString("dd-MM-yyyy HH:mm");
            lblLlegada.Text = b.Vuelo.TLlegada.ToString("dd-MM-yyyy HH:mm");
        }
    }
}
