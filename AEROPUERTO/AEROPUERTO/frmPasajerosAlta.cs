﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class frmPasajerosAlta : Form
    {
        public frmPasajerosAlta()
        {
            InitializeComponent();
        }

        void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = (from BE.PASAJERO p in BLL.PASAJERO.Listar()
                                        where p.Estado == 1
                                        select p).ToList();
        }
        private void btnAlta_Click(object sender, EventArgs e)
        {
            if(VALIDADOR.ValidaText(txtApellido.Text) && VALIDADOR.ValidaText(txtNombre.Text) && VALIDADOR.ValidaText(txtPasaporte.Text) && dtFechaNacimiento.Value < DateTime.Now)
            {
                BE.PASAJERO pasajero = new BE.PASAJERO(0, txtNombre.Text, txtApellido.Text, txtPasaporte.Text, dtFechaNacimiento.Value, 1);

                Tuple<bool, string> resultado = BLL.PASAJERO.Escribir(pasajero);
                if (resultado.Item1)
                {
                    Enlazar();
                }
                else
                {
                    MessageBox.Show(resultado.Item2);
                }
            }
            else
            {
                MessageBox.Show("Error al validar los datos.");
            }
        }

        private void frmPasajerosAlta_Load(object sender, EventArgs e)
        {
            Enlazar();
        }
    }
}
