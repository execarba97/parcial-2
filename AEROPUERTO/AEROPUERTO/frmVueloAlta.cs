﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class frmVueloAlta : Form
    {
        List<BE.ESCALA> escalas = new List<BE.ESCALA>();
        public frmVueloAlta()
        {
            InitializeComponent();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void frmVueloAlta_Load(object sender, EventArgs e)
        {
            Enlazar();
            dtEmbarco.Format = DateTimePickerFormat.Custom;
            dtEmbarco.CustomFormat = "dd/MM/yyyy HH:mm";

            dtLlegada.Format = DateTimePickerFormat.Custom;
            dtLlegada.CustomFormat = "dd/MM/yyyy HH:mm";
        }

        void Enlazar()
        {
            cbAvion.DataSource = null;
            cbAvion.DataSource = BLL.AVION.Listar();

            cbDestino.DataSource = null;
            cbDestino.DataSource = (from BE.CIUDAD c in BLL.CIUDAD.Listar()
                                    where c.Estado == 1
                                    select c).ToList();

            cbOrigen.DataSource = null;
            cbOrigen.DataSource = BLL.CIUDAD.Listar();

            dtgVuelos.DataSource = null;
            dtgVuelos.DataSource = BLL.VUELO.Listar();
        }

        private void btnAlta_Click(object sender, EventArgs e)
        {
            if(dtLlegada.Value > dtEmbarco.Value && (BE.AVION)cbAvion.SelectedItem != null && ((BE.CIUDAD)cbOrigen.SelectedItem) != null && ((BE.CIUDAD)cbDestino.SelectedItem) != null)
            {
                BE.VUELO vuelo = new BE.VUELO(0, (BE.AVION)cbAvion.SelectedItem, "", 1, ((BE.CIUDAD)cbOrigen.SelectedItem).ID, ((BE.CIUDAD)cbDestino.SelectedItem).ID, dtLlegada.Value, dtEmbarco.Value);
                Tuple<bool, string> resultado = BLL.VUELO.Escribir(vuelo);

                if (resultado.Item1)
                {
                    vuelo = (from BE.VUELO v in BLL.VUELO.Listar()
                             where v.Avion.Patente == ((BE.AVION)cbAvion.SelectedItem).Patente && v.IdCiudadOrigen == ((BE.CIUDAD)cbOrigen.SelectedItem).ID && v.IdCiudadDestino == ((BE.CIUDAD)cbDestino.SelectedItem).ID && v.TEmbarco.ToString("yyyy-MM-dd HH:mm") == dtEmbarco.Value.ToString("yyyy-MM-dd HH:mm") && v.TLlegada.ToString("yyyy-MM-dd HH:mm") == dtLlegada.Value.ToString("yyyy-MM-dd HH:mm")
                             select v).FirstOrDefault();

                    foreach (BE.ESCALA esc in escalas)
                    {
                        esc.CambiarVuelo(vuelo.ID);
                        BLL.ESCALA.Escribir(esc);
                    }

                    MessageBox.Show("Vuelo cargado correctamente.");
                    Enlazar();
                }
                else
                {
                    MessageBox.Show(resultado.Item2);
                }
            }
            else
            {
                MessageBox.Show("Verifique la carga de datos.");
            }
        }

        private void btnAgregarEscala_Click(object sender, EventArgs e)
        {
            frmAltaEscalas frm = new frmAltaEscalas();
            frm.RecibirDatos(escalas, dtLlegada.Value, dtEmbarco.Value);
            frm.Show();
        }
    }
}
