﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class frmDestinoVisualizar : Form
    {
        public frmDestinoVisualizar()
        {
            InitializeComponent();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            BE.CIUDAD ciudad = (BE.CIUDAD)dataGridView1.CurrentRow.DataBoundItem;
            BLL.CIUDAD.MostrarEnMaps(ciudad);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            BE.CIUDAD ciudad = (BE.CIUDAD)dataGridView1.CurrentRow.DataBoundItem;

            lblPais.Text = ciudad.Pais;
            lblCiudad.Text = ciudad.Ciudad;
            lblCN.Text = ciudad.CoordNorte;
            lblCS.Text = ciudad.CoordSur;
        }

        void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = (from BE.CIUDAD c in BLL.CIUDAD.Listar()
                                        where c.Estado == 1
                                        select c).ToList();
        }

        private void frmDestinoVisualizar_Load(object sender, EventArgs e)
        {
            Enlazar();
        }
    }
}
