﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class frmAltaEscalas : Form
    {
        List<BE.ESCALA> escalas = new List<BE.ESCALA>();
        DateTime llegada, partida;

        public frmAltaEscalas()
        {
            InitializeComponent();
        }

        private void btnAgregarEscala_Click(object sender, EventArgs e)
        {
            if(dtLlegada.Value < dtPartida.Value)
            {
                BE.ESCALA escala = new BE.ESCALA(0, ((BE.CIUDAD)cbCiudades.SelectedItem).ID, dtLlegada.Value, dtPartida.Value, 0);

                escalas.Add(escala);
                this.Close();
            }
            else
            {
                MessageBox.Show("La fecha de llegada debe ser menor a la fecha de partida.");
            }
        }

        void Enlazar()
        {
            cbCiudades.DataSource = null;
            cbCiudades.DataSource = (from BE.CIUDAD c in BLL.CIUDAD.Listar()
                                     where c.Estado == 1
                                     select c).ToList();
        }

        private void dtLlegada_ValueChanged(object sender, EventArgs e)
        {

        }

        private void frmAltaEscalas_Load(object sender, EventArgs e)
        {
            dtPartida.Format = DateTimePickerFormat.Custom;
            dtPartida.CustomFormat = "dd/MM/yyyy HH:mm:ss";

            dtLlegada.Format = DateTimePickerFormat.Custom;
            dtLlegada.CustomFormat = "dd/MM/yyyy HH:mm:ss";

            dtLlegada.MaxDate = llegada;
            dtLlegada.MinDate = partida;
            dtPartida.MaxDate = llegada;
            dtPartida.MinDate = partida;

            Enlazar();
        }

        public void RecibirDatos(List<BE.ESCALA> escala, DateTime llega, DateTime parte)
        {
            llegada = llega;
            partida = parte;
            escalas = escala;
        }
    }
}
