﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class frmPasajerosBaja : Form
    {
        public frmPasajerosBaja()
        {
            InitializeComponent();
        }

        void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = (from BE.PASAJERO p in BLL.PASAJERO.Listar()
                                        where p.Estado == 1
                                        select p).ToList();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            BE.PASAJERO pasajero = (BE.PASAJERO)dataGridView1.CurrentRow.DataBoundItem;
            if (pasajero != null)
            {
                Tuple<bool, string> resultado = BLL.PASAJERO.Borrar(pasajero);
                if (resultado.Item1)
                {
                    Enlazar();
                }
                else
                {
                    MessageBox.Show(resultado.Item2);
                }
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            BE.PASAJERO pasajero = (BE.PASAJERO)dataGridView1.CurrentRow.DataBoundItem;

            lblNombre.Text = pasajero.Nombre;
            lblApellido.Text = pasajero.Apellido;
            lblPasaporte.Text = pasajero.Pasaporte;
            lblFnac.Text = pasajero.FechaNacimiento.ToString();
        }

        private void frmPasajerosBaja_Load(object sender, EventArgs e)
        {
            Enlazar();
        }
    }
}
