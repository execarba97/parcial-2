﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class frmDestinoModificar : Form
    {
        public frmDestinoModificar()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            BE.CIUDAD ciudad = (BE.CIUDAD)dataGridView1.CurrentRow.DataBoundItem;

            txtPais.Text = ciudad.Pais;
            txtCiudad.Text = ciudad.Ciudad;
            txtCN.Text = ciudad.CoordNorte;
            txtCS.Text = ciudad.CoordSur;
        }

        void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = (from BE.CIUDAD c in BLL.CIUDAD.Listar()
                                        where c.Estado == 1
                                        select c).ToList();
        }

        private void frmDestinoModificar_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if(VALIDADOR.ValidaText(txtPais.Text) && VALIDADOR.ValidaText(txtCiudad.Text) && VALIDADOR.ValidaText(txtCS.Text) && VALIDADOR.ValidaText(txtCN.Text))
            {
                BE.CIUDAD ciudadAux = (BE.CIUDAD)dataGridView1.CurrentRow.DataBoundItem;
                BE.CIUDAD ciudad = new BE.CIUDAD(ciudadAux.ID, txtPais.Text, txtCiudad.Text, txtCN.Text, txtCS.Text, 1);
                Tuple<bool, string> resultado = BLL.CIUDAD.Escribir(ciudad);
                if (resultado.Item1)
                {
                    Enlazar();
                }
                else
                {
                    MessageBox.Show(resultado.Item2);
                }
            }
        }
    }
}
