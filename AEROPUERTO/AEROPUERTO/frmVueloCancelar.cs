﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class frmVueloCancelar : Form
    {
        public frmVueloCancelar()
        {
            InitializeComponent();
        }

        void Enlazar()
        {
            dtgVuelos.DataSource = null;
            dtgVuelos.DataSource = BLL.VUELO.Listar();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            if((BE.VUELO)dtgVuelos.CurrentRow.DataBoundItem != null)
            {
                Tuple<bool, string> resultado = BLL.VUELO.Cancelar((BE.VUELO)dtgVuelos.CurrentRow.DataBoundItem);
                if (resultado.Item1)
                {
                    Enlazar();
                }
                else
                {
                    MessageBox.Show(resultado.Item2);
                }
            }
            else
            {
                MessageBox.Show("Seleccione un vuelo.");
            }
        }

        private void dtgVuelos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            BE.VUELO vuelo = (BE.VUELO)dtgVuelos.CurrentRow.DataBoundItem;

            lblAvion.Text = (vuelo.Avion).ToString();
            lblDestino.Text = (BLL.CIUDAD.BuscarCiudad(vuelo.IdCiudadDestino)).ToString();
            lblOrigen.Text = (BLL.CIUDAD.BuscarCiudad(vuelo.IdCiudadOrigen)).ToString();
            lblEmbarco.Text = vuelo.TEmbarco.ToString();
            lblLlegada.Text = vuelo.TLlegada.ToString();
        }

        private void frmVueloCancelar_Load(object sender, EventArgs e)
        {
            Enlazar();
        }
    }
}
