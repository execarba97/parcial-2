﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class AVION
    {
        public AVION(int I, int capac, string marc, string aero, string matricula)
        {
            id = I;
            capacidad = capac;
            marca = marc;
            aerolinea = aero;
            patente = matricula;
        }

        private int id;

        public int ID
        {
            get { return id; }
        }

        private int capacidad;

        public int Capacidad
        {
            get { return capacidad; }
        }

        private string marca;

        public string Marca
        {
            get { return marca; }
        }

        private string patente;

        public string Patente
        {
            get { return patente; }
        }

        private string aerolinea;

        public string Aerolinea
        {
            get { return aerolinea; }
        }

        public override string ToString()
        {
            return patente;
        }
    }
}
