﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class ESCALA
    {
        public ESCALA(int i, int idciudad, DateTime tarribo, DateTime tpartid, int vuelo)
        {
            id = i;
            idDestino = idciudad;
            tLlegada = tarribo;
            tPartida = tpartid;
            idVuelo = vuelo;
        }

        private int id;

        public int ID
        {
            get { return id; }
        }

        private int idDestino;

        public int IDDestino
        {
            get { return idDestino; }
        }

        private DateTime tLlegada;

        public DateTime TLlegada
        {
            get { return tLlegada; }
        }

        private DateTime tPartida;

        public DateTime TPartida
        {
            get { return tPartida; }
        }

        private int idVuelo;

        public int IDVuelo
        {
            get { return idVuelo; }
        }

        public void CambiarVuelo(int vuelo)
        {
            idVuelo = vuelo;
        }
    }
}