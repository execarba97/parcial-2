﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class BOLETO
    {
        public BOLETO()
        {

        }
        public BOLETO(int i, int pasajero, int origen, int destino, float prec, DateTime salida, DateTime llegada, VUELO vue, int state)
        {
            id = i;
            idPasajero = pasajero;
            idOrigen = origen;
            idDestino = destino;
            precio = prec;
            horaSalida = salida;
            horaLlegada = llegada;
            vuelo = vue;
            estado = state;
        }
        private int id;

        public int ID
        {
            get { return id; }
        }

        private int idPasajero;

        public int IDPasajero
        {
            get { return idPasajero; }
        }

        private int idOrigen;

        public int IDOrigen
        {
            get { return idOrigen; }
        }

        private int idDestino;

        public int IDDestino
        {
            get { return idDestino; }
        }

        private float precio;

        public float Precio
        {
            get { return precio; }
        }

        private DateTime horaSalida;
        public DateTime HoraSalida
        {
            get { return horaSalida; }
        }

        private DateTime horaLlegada;

        public DateTime HoraLlegada
        {
            get { return horaLlegada; }
        }

        private int estado;

        public int Estado
        {
            get { return estado; }
        }


        private VUELO vuelo;

        public VUELO Vuelo
        {
            get { return vuelo; }
        }
    }
}