﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class VUELO
    {
        public VUELO(int i, AVION av, string tip, int state, int origen, int destino, DateTime llegada, DateTime partida)
        {
            id = i;
            avion = av;
            tipo = tip;
            estado = state;
            idCiudadOrigen = origen;
            idCiudadDestino = destino;
            tLlegada = llegada;
            tEmbarco = partida;
        }

        private int id;

        public int ID
        {
            get { return id; }
        }

        private AVION avion;

        public AVION Avion
        {
            get { return avion; }
        }

        private string tipo;

        public string Tipo
        {
            get { return tipo; }
        }

        private int estado;

        public int Estado
        {
            get { return estado; }
        }

        private int idCiudadOrigen;

        public int IdCiudadOrigen
        {
            get { return idCiudadOrigen; }
        }

        private int idCiudadDestino;

        public int IdCiudadDestino
        {
            get { return idCiudadDestino; }
        }

        private DateTime tLlegada;

        public DateTime TLlegada
        {
            get { return tLlegada; }
        }

        private DateTime tEmbarco;

        public DateTime TEmbarco
        {
            get { return tEmbarco; }
        }

        public void CambiarTipo(string tip)
        {
            tipo = tip;
        }

        public override string ToString()
        {
            return avion.Patente;
        }
    }
}