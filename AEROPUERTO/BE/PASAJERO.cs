﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class PASAJERO
    {
        public PASAJERO(int i, string nom, string ape, string pasap, DateTime fnacimiento, int state)
        {
            id = i;
            nombre = nom;
            apellido = ape;
            pasaporte = pasap;
            fechaNacimiento = fnacimiento;
            estado = state;
        }

        private int id;

        public int ID
        {
            get { return id; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
        }

        private string apellido;

        public string Apellido
        {
            get { return apellido; }
        }

        private string pasaporte;

        public string Pasaporte
        {
            get { return pasaporte; }
        }

        private DateTime fechaNacimiento;

        public DateTime FechaNacimiento
        {
            get { return fechaNacimiento; }
        }

        private int estado;

        public int Estado
        {
            get { return estado; }
        }

        public override string ToString()
        {
            return nombre + ' ' + apellido;
        }
    }
}