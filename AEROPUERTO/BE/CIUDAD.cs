﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class CIUDAD
    {
        public CIUDAD(int i, string pai, string ciu, string cn, string cs, int state)
        {
            id = i;
            pais = pai;
            ciudad = ciu;
            coordNorte = cn;
            coordSur = cs;
            estado = state;
        }

        private int id;

        public int ID
        {
            get { return id; }
        }

        private string pais;

        public string Pais
        {
            get { return pais; }
        }

        private string ciudad;

        public string Ciudad
        {
            get { return ciudad; }
        }

        private string coordNorte;

        public string CoordNorte
        {
            get { return coordNorte; }
        }

        private string coordSur;

        public string CoordSur
        {
            get { return coordSur; }
        }

        private int estado;

        public int Estado
        {
            get { return estado; }
        }

        public override string ToString()
        {
            return Ciudad + ", " + Pais;
        }
    }
}