﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace BLL
{
    public class CIUDAD
    {
        public static List<BE.CIUDAD> Listar()
        {
            DAL.MP_CIUDAD mp = new DAL.MP_CIUDAD();

            return mp.Listar();
        }

        public static BE.CIUDAD BuscarCiudad(int id)
        {
            return (from BE.CIUDAD c in Listar()
                    where c.ID == id
                    select c).FirstOrDefault();
        }

        public static Tuple<bool, string> Escribir(BE.CIUDAD ciudad)
        {
            DAL.MP_CIUDAD mp = new DAL.MP_CIUDAD();
            List<BE.CIUDAD> ciudades = Listar();

            BE.CIUDAD c = (from BE.CIUDAD city in ciudades
                           where city.Ciudad == ciudad.Ciudad && city.Pais == ciudad.Pais
                           select city).FirstOrDefault();

            if (c == null)
            {
                return mp.Alta(ciudad);
            }
            else
            {
                return mp.Modificar(ciudad);
            }
        }

        public static Tuple<bool, string> Borrar(BE.CIUDAD ciudad)
        {
            DAL.MP_CIUDAD mp = new DAL.MP_CIUDAD();
            return mp.Baja(ciudad);
        }

        public static void MostrarEnMaps(BE.CIUDAD ciudad)
        {
            Process.Start("chrome", "https://www.google.com.ar/maps/@" + ciudad.CoordNorte + "," + ciudad.CoordSur + ",11.78z");
        }
    }
}