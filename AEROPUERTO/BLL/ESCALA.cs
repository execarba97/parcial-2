﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class ESCALA
    {
        public static List<BE.ESCALA> Listar()
        {
            DAL.MP_ESCALA mp = new DAL.MP_ESCALA();

            return mp.Listar();
        }

        public static Tuple<bool, string> Escribir(BE.ESCALA escala)
        {
            DAL.MP_ESCALA mp = new DAL.MP_ESCALA();

            return mp.Alta(escala);
        }
    }
}