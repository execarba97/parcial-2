﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class BOLETO
    {
        public static List<BE.BOLETO> Listar()
        {
            DAL.MP_BOLETO mp = new DAL.MP_BOLETO();

            return mp.Listar();
        }

        public static Tuple<bool, string> Escribir(BE.BOLETO boleto)
        {
            DAL.MP_BOLETO mp = new DAL.MP_BOLETO();

            return mp.Alta(boleto);
        }
    }
}
