﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class VUELO
    {
        public static List<BE.VUELO> Listar()
        {
            DAL.MP_VUELO mp = new DAL.MP_VUELO();
            return mp.Listar();
        }

        public static Tuple<bool, string> Escribir(BE.VUELO vuelo)
        {
            DAL.MP_VUELO mp = new DAL.MP_VUELO();

            BE.VUELO v1 = (from BE.VUELO vue in Listar()
                          where vue.Avion.Patente == vuelo.Avion.Patente && ((vue.TEmbarco >= vuelo.TLlegada && vue.TLlegada <= vuelo.TLlegada) || (vue.TEmbarco >= vuelo.TEmbarco && vue.TLlegada <= vuelo.TEmbarco))
                          select vue).FirstOrDefault();

            if (v1 == null)
            {

                if(CIUDAD.BuscarCiudad(vuelo.IdCiudadOrigen).Pais.ToUpper() == "ARGENTINA" && CIUDAD.BuscarCiudad(vuelo.IdCiudadDestino).Pais.ToUpper() == "ARGENTINA")
                {
                    vuelo.CambiarTipo("Nacional");
                }
                else
                {
                    vuelo.CambiarTipo("Internacional");
                }

                return mp.Alta(vuelo);
            }
            else
            {
                Tuple<bool, string> resultado = new Tuple<bool, string>(false, "El vuelo ya existe.");
                return resultado;
            }
        }

        public static Tuple<bool, string> Cancelar(BE.VUELO vuelo)
        {
            DAL.MP_VUELO mp = new DAL.MP_VUELO();
            return mp.Cancelar(vuelo);
        }
    }
}