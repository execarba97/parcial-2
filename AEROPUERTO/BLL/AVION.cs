﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class AVION
    {
        public static Tuple<bool, string> Escribir(BE.AVION avion)
        {
            DAL.MP_AVION mp = new DAL.MP_AVION();
            
            return mp.Alta(avion);
        }

        public static List<BE.AVION> Listar()
        {
            DAL.MP_AVION mp = new DAL.MP_AVION();

            return mp.Listar();
        }
    }
}