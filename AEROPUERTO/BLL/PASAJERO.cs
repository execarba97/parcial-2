﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class PASAJERO
    {
        public static List<BE.PASAJERO> Listar()
        {
            DAL.MP_PASAJERO mp = new DAL.MP_PASAJERO();
            return mp.Listar();
        }

        public static Tuple<bool, string> Escribir(BE.PASAJERO pasajero)
        {
            DAL.MP_PASAJERO mp = new DAL.MP_PASAJERO();

            BE.PASAJERO pa = (from BE.PASAJERO p in Listar()
                              where p.Pasaporte == pasajero.Pasaporte
                              select p).FirstOrDefault();

            if (pa == null)
            {
                return mp.Alta(pasajero);
            }
            else
            {
                return new Tuple<bool, string>(false, "El pasajero ya existe");
            }
        }

        public static Tuple<bool, string> Modificar(BE.PASAJERO pasajero)
        {
            DAL.MP_PASAJERO mp = new DAL.MP_PASAJERO();

            return mp.Modificar(pasajero);
        }

        public static Tuple<bool, string> Borrar(BE.PASAJERO pasajero)
        {
            DAL.MP_PASAJERO mp = new DAL.MP_PASAJERO();

            return mp.Baja(pasajero);
        }
    }
}